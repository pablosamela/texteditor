var Synonyms = function(container) {
  const maxRelatedWords = 6;
  function getRelatedWords(){
    let relatedWord = getWord();
    fetch("https://api.datamuse.com/words?rel_syn=" + relatedWord)
    .then(res => res.json())
    .then(data => {
      if(data.length) {
        createSynonyms(data);
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  function createSynonyms(synonymsList){
    let slicedData =  synonymsList.slice(0, maxRelatedWords);
    for(let word of slicedData){
      container.append(
        $('<a>', { class: 'word', href: '#'}).attr('data-Word' , word.word ).text(word.word)
      );
    }
    container.on('click','.word',function(e){
      e.preventDefault();
      replaceWord($(this).data('word'));
    });
  }

  function getWord() {
    let selection, word = "";
    if (window.getSelection && (selection = window.getSelection()).modify) {
        var selectedRange = selection.getRangeAt(0);
        selection.collapseToStart();
        selection.modify("move", "backward", "word");
        selection.modify("extend", "forward", "word");
        word = selection.toString();
        selection.removeAllRanges();
        selection.addRange(selectedRange);
    } else if ( (selection = document.selection) && selection.type != "Control") {
        var range = selection.createRange();
        range.collapse(true);
        range.expand("word");
        word = range.text;
    }
    return word;
  }

  function replaceWord(whichWord){     
    selection = document.getSelection();
    selection.modify("extend", "backward", "word");
    range = selection.getRangeAt(0);
    range.deleteContents();
    var el = document.createElement("div");
    el.innerHTML = whichWord;
    var frag = document.createDocumentFragment(), node;
    while (node = el.firstChild) {
      frag.appendChild(node);
    }
    range.insertNode(frag);
    range.collapse();
  }
  return {
    getRelatedWords: getRelatedWords,

  }
}