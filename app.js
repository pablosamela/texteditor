jQuery(document).ready(function () { 
    
  // you can add commands to actions array and to buttons in html 
  const actions = ['bold','italic','underline']; 

  const ediatableContainer = $('#file');
  const controPanel =$('#control-panel');
  const relatedWordsContainer = $('#related-words');

  controPanel.on('click','.format-action',function() {
    const argument = $(this).data('argument');
    document.execCommand($(this).data('command'),false, argument ? argument : null);
  });
  
  ediatableContainer.on('click blur keyup',setButtonsStatus);

  ediatableContainer.on('input', function(){
    setButtonsStatus();
    relatedWordsContainer.empty();
    const synonyms = new Synonyms(relatedWordsContainer);
    synonyms.getRelatedWords();
  });

  function setButtonsStatus(){
    actions.map(function(action){
      const buttonAction =  $('#format-actions').find(`button[data-command='${action}']`);
      if (document.queryCommandState(action)){
        buttonAction.addClass('selected');
      } else {
        buttonAction.removeClass('selected');
      }
    });
  }

});




